package edu.emory.oit.vpcplanding.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Constants implements IsSerializable {

	public final static long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
	public static final String USER_ACCOUNT = "UserAccount";
	public static final String SERVICE_SUMMARY = "ServiceSummary";
	public static final String NET_ID = "NetId=";
	public static final String SESSION_TIMEOUT = "SessionTimeout";
	public static final String STYLE_INFO_POPUP_MESSAGE = "informationalPopupMessage";
	public static final String MOA_SERVICE = "Service.v1_0";
	public static final String MOA_SERVICE_QUERY_SPEC = "ServiceQuerySpecification.v1_0";
	public static final String MOA_SVC_SECURITY_ASSESSMENT = "ServiceSecurityAssessment.v1_0";
	public static final String MOA_SVC_SECURITY_ASSESSMENT_QUERY_SPEC = "ServiceSecurityAssessmentQuerySpecification.v1_0";
	public static final String TRUE = "true";
	public static final String FALSE = "false";

}
