package edu.emory.oit.vpcplanding.shared;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AWSServiceQueryFilterPojo extends SharedObject implements IsSerializable, QueryFilter {
	String serviceId;
	String awsServiceCode;
	String awsServiceName;
	String awsStatus;
	String siteStatus;
	List<String> categories = new java.util.ArrayList<String>();
	List<String> consoleCategories = new java.util.ArrayList<String>();
	String awsHipaaEligible;
	String siteHipaaEligible;
	List<AWSTagPojo> tags = new java.util.ArrayList<AWSTagPojo>(); 
	boolean fuzzyFilter=false;

	public AWSServiceQueryFilterPojo() {
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getAwsServiceCode() {
		return awsServiceCode;
	}

	public void setAwsServiceCode(String awsServiceCode) {
		this.awsServiceCode = awsServiceCode;
	}

	public String getAwsServiceName() {
		return awsServiceName;
	}

	public void setAwsServiceName(String awsServiceName) {
		this.awsServiceName = awsServiceName;
	}

	public String getAwsStatus() {
		return awsStatus;
	}

	public void setAwsStatus(String awsStatus) {
		this.awsStatus = awsStatus;
	}

	public String getSiteStatus() {
		return siteStatus;
	}

	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getConsoleCategories() {
		return consoleCategories;
	}

	public void setConsoleCategories(List<String> consoleCategories) {
		this.consoleCategories = consoleCategories;
	}

	public String getAwsHipaaEligible() {
		return awsHipaaEligible;
	}

	public void setAwsHipaaEligible(String awsHipaaEligible) {
		this.awsHipaaEligible = awsHipaaEligible;
	}

	public String getSiteHipaaEligible() {
		return siteHipaaEligible;
	}

	public void setSiteHipaaEligible(String siteHipaaEligible) {
		this.siteHipaaEligible = siteHipaaEligible;
	}

	public List<AWSTagPojo> getTags() {
		return tags;
	}

	public void setTags(List<AWSTagPojo> tags) {
		this.tags = tags;
	}

	public boolean isFuzzyFilter() {
		return fuzzyFilter;
	}

	public void setFuzzyFilter(boolean fuzzyFilter) {
		this.fuzzyFilter = fuzzyFilter;
	}

}
