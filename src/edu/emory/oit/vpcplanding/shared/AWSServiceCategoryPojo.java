package edu.emory.oit.vpcplanding.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

@SuppressWarnings("serial")
public class AWSServiceCategoryPojo extends SharedObject implements IsSerializable {
	String name;
	String code;

	public AWSServiceCategoryPojo() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
