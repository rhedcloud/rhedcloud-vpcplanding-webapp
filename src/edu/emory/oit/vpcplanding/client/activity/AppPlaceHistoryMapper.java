package edu.emory.oit.vpcplanding.client.activity;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingPlace;

@WithTokenizers({MainLandingPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {

}
