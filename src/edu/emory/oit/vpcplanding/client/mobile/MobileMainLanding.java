package edu.emory.oit.vpcplanding.client.mobile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import edu.emory.oit.vpcplanding.client.presenter.ViewImplBase;
import edu.emory.oit.vpcplanding.client.presenter.landing.MainLandingView;
import edu.emory.oit.vpcplanding.shared.UserAccountPojo;

public class MobileMainLanding extends ViewImplBase implements MainLandingView {
	Presenter presenter;
	UserAccountPojo userLoggedIn;


	private static MobileMainLandingUiBinder uiBinder = GWT.create(MobileMainLandingUiBinder.class);

	interface MobileMainLandingUiBinder extends UiBinder<Widget, MobileMainLanding> {
	}

	public MobileMainLanding() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setInitialFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Widget getStatusMessageSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void applyEmoryAWSAdminMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void applyEmoryAWSAuditorMask() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUserLoggedIn(UserAccountPojo user) {
		this.userLoggedIn = user;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setReleaseInfo(String releaseInfoHTML) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hidePleaseWaitPanel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showPleaseWaitPanel() {
		// TODO Auto-generated method stub
		
	}

}
