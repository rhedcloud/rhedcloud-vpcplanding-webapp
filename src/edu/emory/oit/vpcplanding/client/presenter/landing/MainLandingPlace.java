package edu.emory.oit.vpcplanding.client.presenter.landing;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class MainLandingPlace extends Place {
	/**
	 * The tokenizer for this place.
	 */
	@Prefix("mainLanding")
	public static class Tokenizer implements PlaceTokenizer<MainLandingPlace> {

		private static final String LANDING = "landing";

		public MainLandingPlace getPlace(String token) {
			if (token != null) {
				return new MainLandingPlace();
			}
			else {
				// If the ID cannot be parsed, assume we are creating a caseRecord.
				return MainLandingPlace.getMainLandingPlace();
			}
		}

		public String getToken(MainLandingPlace place) {
			return LANDING;
		}
	}

	/**
	 * The singleton instance of this place used for creation.
	 */
	private static MainLandingPlace singleton;

	/**
	 * Create an instance of this place
	 * 
	 * @param mrn the ID of the caseRecord to edit
	 * @param caseRecord the caseRecord to edit, or null if not available
	 * @return the place
	 */
	public static MainLandingPlace createMainLandingPlace() {
		return new MainLandingPlace();
	}

	/**
	 * Get the singleton instance of this place
	 * 
	 * @return the place
	 */
	public static MainLandingPlace getMainLandingPlace() {
		if (singleton == null) {
			singleton = new MainLandingPlace();
		}
		return singleton;
	}
}
